<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $title
 * @property string $slug
 * @property integer $categoty_id
 * @property string $content
 * @property string $image_token
 * @property integer $create_at
 * @property integer $update_at
 *
 * @property Category $categoty
 */
class Page extends \yii\db\ActiveRecord
{
    const IMAGE_PATH = '/web/uploads/page/gallery/';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
            'Sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'categoty_id', 'content'], 'required'],
            [['categoty_id', 'create_at', 'update_at'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 128],
            [['image_token'], 'string', 'max' => 32],
            [['content'], 'string'],
            [['categoty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoty_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'slug' => 'Ссылка',
            'categoty_id' => 'Категория',
            'content' => 'Содержимое страницы',
            'create_at' => 'Дата добавления',
            'update_at' => 'Дата редактирования',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoty_id']);
    }

    /** @inheritdoc */
    public function getImages()
    {
        if(!isset($this->image_token))
            return [];
        else {
            $files = glob(Yii::getAlias('@frontend' .  self::IMAGE_PATH . $this->image_token . '/')."*.{jpg,png,gif}", GLOB_BRACE);
            $return = array();
            foreach ($files as $file) {
                $return[] = array_pop(explode('/', $file));
            }
            return $return;
        }
    }
}
