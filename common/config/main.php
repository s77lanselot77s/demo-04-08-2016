<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',

        ],
        'urlManager'=>[
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl'=>true,
            'showScriptName'=>false,
            'scriptUrl'=>'/index.php',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => '111.111.111.111',
                'username' => 'mail@mail.ru',
                'password' => 'pass',
                'port' => '587',
            ],*/
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y H:i:s',
            'timeFormat' => 'php:H:i:s',
            'timeZone' => 'Europe/Moscow',
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            // following line will restrict access to admin page
            //'as backend' => 'dektrium\user\filters\BackendFilter',
            'mailer' => [

            ],
            'enableConfirmation'=>false,
            'enableRegistration'=>false,
            'modelMap' => [
                 'User' => 'common\models\user\User',
                /*'UserSearch' => 'backend\models\user\UserSearch',
                'Profile' => 'backend\models\user\Profile',
                'LoginForm' => 'backend\models\user\LoginForm',*/
            ],
            'controllerMap' => [
                /* 'admin' => 'backend\controllers\user\AdminController',
                 'profile' => 'backend\controllers\user\ProfileController',
                 'settings' => 'backend\controllers\user\SettingsController',
                 'security' => 'backend\controllers\user\SecurityController',*/
            ],
            'admins' => ['administrator'],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
        ],
    ],
];
