<?php

namespace backend\controllers;

use common\models\PageImage;
use Yii;
use common\models\Page;
use common\models\search\PageSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\imagine\Image;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/frontend/web/uploads/page/content/', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/page/content/' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Images upload for Page.
     * @return mixed
     */
    public function actionUpload()
    {
        $fileName = 'file';
        $uploadPath = Yii::getAlias('@frontend') .  Page::IMAGE_PATH . Yii::$app->session->get('page_image');
        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);
            $image = Image::getImagine()->open($file->tempName);
            FileHelper::createDirectory($uploadPath);
            //Print file data
            $ext = array_pop(explode('.', $_FILES[$fileName]['name']));
            //var_dump($file);die;
            $newName = Yii::$app->security->generateRandomString(8);
            $image->resize($image->getSize()->widen(500));
            if ($image->save($uploadPath . '/' . $newName . '.' . $ext)) {
                echo \yii\helpers\Json::encode($file);
            }
        }

        return false;
    }

    /**
     * Images delete for Page.
     * @return mixed
     */
    public function actionImageDelete()
    {
        $token = $_POST['token'];
        $image = $_POST['image'];

        try {
            unlink(Yii::getAlias('@frontend' .  Page::IMAGE_PATH . $token . '/' . $image));
        } catch(\Exception $e) {}

        return true;

    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();
        Yii::$app->session->set('page_image', Yii::$app->security->generateRandomString(10));
        if ($model->load(Yii::$app->request->post())) {
            $model->image_token = Yii::$app->session->get('page_image');
             Yii::$app->session->remove('page_image');
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(isset($model->image_token))
            Yii::$app->session->set('page_image', $model->image_token);
        else
            Yii::$app->session->set('page_image', Yii::$app->security->generateRandomString(10));

        if ($model->load(Yii::$app->request->post())) {
            $model->image_token = Yii::$app->session->get('page_image');
            Yii::$app->session->remove('page_image');
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($this->findModel($id)->delete());
        $model = $this->findModel($id);
        if(isset($model->image_token))
        {
            try {
                FileHelper::removeDirectory(Yii::getAlias('@frontend' .  Page::IMAGE_PATH . $model->image_token));
            } catch(\Exception $e) {}
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
