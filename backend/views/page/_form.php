<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Category;
use yii\helpers\ArrayHelper;
use vova07\imperavi\Widget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */




?>

<div class="page-form">

    <?php $form = ActiveForm::begin([
        'id' => 'page',
        'options' => ['enctype'=>'multipart/form-data'],
    ]); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'categoty_id')->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'title'), ['prompt' => 'Выберите категорию']) ?>

    <?= $form->field($model, 'content')->widget(Widget::classname(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'pastePlainText' => true,
            'buttonSource' => true,
            'buttonsHide' => ['file'],
            'plugins' => [
                //'clips',
                'fullscreen',
                'video'
            ],
            'imageUpload' => Url::to(['page/image-upload']),
            'imageManagerJson' => Url::to(['page/images-get']),
            'fileManagerJson' => Url::to(['/blog/files-get']),
            'fileUpload' => Url::to(['/blog/file-upload'])
        ]
    ]); ?>

    <?php if(Yii::$app->controller->action->id == 'update'): ?>
        <?php \yii\widgets\Pjax::begin(['id' => 'pjax-image']); ?>
        <?php $this->registerJs("
            $('.delete-image').on('click', function(){
            $.ajax({
               type: 'POST',
               url: '/admin/page/image-delete',
               data: {'token': $(this).attr('data-token'), 'image': $(this).attr('data-image')},
               success: function(msg){
                    $.pjax.reload({container:'#pjax-image'});
               }
             });
             return false;
            });
        ", \yii\web\View::POS_READY);

        ?>
        <?php foreach($model->getImages() as $image): ?>
            <div class="image-block">
                <?php $img = '/frontend' . \common\models\Page::IMAGE_PATH . $model->image_token . '/' . $image; ?>
                <?= Html::a(Html::img($img, ['class' => 'img-responsive img-thumbnail image-update']), $img, ['class' => 'fancybox', 'data-pjax' => '0', 'rel' => 'gallery']) ?>
                <?= Html::a('[удалить]', null, ['class' => 'delete-image', 'data-token' => $model->image_token, 'data-image' => $image, 'role' => 'button']) ?>
            </div>
        <?php endforeach; ?>
        <?php \yii\widgets\Pjax::end(); ?>
    <?php endif; ?>
    <br><br>
    <?=
    \kato\DropZone::widget([
        'uploadUrl' => '/admin/page/upload',
        'options' => [
            'maxFilesize' => '10',
        ],
        'clientEvents' => [
            'complete' => "function(file){console.log(file)}",
            'removedfile' => "function(file){alert(file.name + ' is removed')}"
        ],
    ]);
    ?>
    <br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
