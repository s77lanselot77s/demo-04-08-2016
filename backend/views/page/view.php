<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Page;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хоттите удалить данную страницу?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="content-block">
        <?= $model->content ?>
    </div>
    <?php $images = $model->getImages(); ?>
    <hr>
    <?php if(!empty($images)): ?>
    <p>Галерея</p>
        <div class="gallery-image">
            <div class="row">
                <?php foreach($model->getImages() as $image): ?>
                    <?php $img = '/frontend' . Page::IMAGE_PATH . $model->image_token . '/' . $image ?>
                    <div class="elem col-lg-3">
                        <?= Html::a(Html::img($img), $img, ['class' => 'fancybox img-thumbnail', 'rel' => 'gallery']) ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    <?php endif; ?>
</div>
