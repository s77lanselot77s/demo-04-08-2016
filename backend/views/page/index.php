<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                [
                    'label' => 'Категория',
                    'attribute'=>'category',
                    'value' => 'category.title',
                ],
                [
                    'attribute' => 'create_at',
                    'value' => function ($model) { return Yii::$app->formatter->asDatetime($model->create_at, 'php:d-m-Y H:i:s', 'date'); }
                ],
                [
                    'attribute' => 'update_at',
                    'value' => function ($model) { return Yii::$app->formatter->asDatetime($model->update_at, 'php:d-m-Y H:i:s', 'date'); }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
