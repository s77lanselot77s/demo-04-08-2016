<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'homeUrl' => '/admin',
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@frontend/runtime/cache',
        ],
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-green',
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@backend/views/user',
                    '@backend/views' => '@dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                ],
            ],
        ],
        'user' => [
            'identityCookie' => [
                'name'     => '_backendIdentity',
                'path'     => '/admin',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'name' => 'BACKENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/admin',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request'=>[
            'baseUrl'=>'/admin',
        ],
        'urlManager'=>[
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl'=>true,
            'showScriptName'=>false,
            //'enableStrictParsing' => true,
            'rules' => [
                '/' => 'site/index',
                '<action:(login|logout)>'     => 'user/security/<action>',
                '<action:(register|resend)>'  => 'user/registration/<action>',
                'confirm/<id:\d+>/<code:\w+>' => 'user/registration/confirm',
                '/forgot'                      => 'user/recovery/request',
                'recover/<id:\d+>/<code:\w+>' => 'user/recovery/reset',
                'settings/<action:\w+>'       => 'user/settings/<action>',
                '<action>'=>'site/<action>',
                '<controller:\w+(-\w+)*>/<id:\d+>' => '<controller>/view',
                '<controller:\w+(-\w+)*>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\\w+(-\w+)*>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>' => '<module>/<controller>/index',
            ],
        ],
    ],
    'modules' => [
        'user' => [
            // following line will restrict access to admin page
            //'as backend' => 'dektrium\user\filters\BackendFilter',
            'mailer' => [

            ],
            'enableConfirmation'=>false,
            'enableRegistration'=>false,
            'modelMap' => [
                 'User' => 'common\models\user\User',
                /* 'UserSearch' => 'backend\models\user\UserSearch',
                 'Profile' => 'backend\models\user\Profile',
                 'LoginForm' => 'backend\models\user\LoginForm',*/
            ],
            'controllerMap' => [
                /* 'admin' => 'backend\controllers\user\AdminController',
                 'profile' => 'backend\controllers\user\ProfileController',
                 'settings' => 'backend\controllers\user\SettingsController',
                 'security' => 'backend\controllers\user\SecurityController',*/
            ],
            'admins' => ['administrator'],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
        ],
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20', '10.110.0.10'] // adjust this to your needs
        ],
    ],
    'params' => $params,
];
