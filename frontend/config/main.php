<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'homeUrl' => '/',
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@frontend/views/user',
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [

            ],
        ],

        'user' => [
            'identityCookie' => [
                'name'     => '_frontendIdentity',
                'path'     => '/',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'name' => 'FRONTENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request'=>[
            'baseUrl'=>'',
        ],
        'urlManager'=>[
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl'=>true,
            'showScriptName'=>false,
            //'enableStrictParsing' => true,
            'rules' => [
                '/' => 'site/index',
                '<action:(login|logout)>'     => 'user/security/<action>',
                '<action:(register|resend)>'  => 'user/registration/<action>',
                'confirm/<id:\d+>/<code:\w+>' => 'user/registration/confirm',
                '/forgot'                      => 'user/recovery/request',
                'recover/<id:\d+>/<code:\w+>' => 'user/recovery/reset',
                'settings/<action:\w+>'       => 'user/settings/<action>',
                '<action>'=>'site/<action>',
                '<controller:\w+(-\w+)*>/<id:\d+>' => '<controller>/view',
                //   '<controller:\w+(-\w+)*>/<action:\w+>/<id:\w+(-\w+)*>' => '<controller>/<action>',
                '<controller:\\w+(-\w+)*>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>' => '<module>/<controller>/index',
            ],

        ],
    ],
    'modules' => [
        'user' => [
            // following line will restrict access to admin page
            //'as backend' => 'dektrium\user\filters\BackendFilter',
            'mailer' => [
            ],
            'enableConfirmation'=>false,
            'enableRegistration'=>false,
            'enableFlashMessages' => false,
            'modelMap' => [
                /*'User' => 'backend\models\user\User',
                'UserSearch' => 'backend\models\user\UserSearch',
                'Profile' => 'backend\models\user\Profile',
                'Account' => 'common\models\user\Account',
                'LoginForm' => 'frontend\models\user\LoginForm',
                'RegistrationForm' => 'frontend\models\user\RegistrationForm',
                'RecoveryForm' => 'frontend\models\user\RecoveryForm',
                'ResendForm' => 'frontend\models\user\ResendForm',*/
            ],
            'controllerMap' => [
                //'admin' => 'backend\controllers\user\AdminController',
                /*'profile' => 'frontend\controllers\user\ProfileController',
                'settings' => 'frontend\controllers\user\SettingsController',
                'security' => 'frontend\controllers\user\SecurityController',
                'registration' => 'frontend\controllers\user\RegistrationController',
                'recovery' => 'frontend\controllers\user\RecoveryController',*/
            ],
            'admins' => ['admin'],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
        ],
    ],
    'params' => $params,
];
